<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

    define("ORB_VERSION", "0.9");
	define("SYSTEM_APPLICATIONS", array("about", "explorer", "settings"));
	define("SYSTEM_DIRECTORIES", array("Desktop"));
	define("DEFAULT_COLOR", "#286090");
	define("PASSWORD_FILE", HOME_DIRECTORIES."/users.txt");
	define("TERMINAL_NETWORK_TIMEOUT", 5);

	/* Orb system backend
	 */
	class orb extends orb_backend {
		public function get_ping() {
			$this->view->add_tag("pong");
		}
	}

	function orb_application_exists($application) {
		$applications = array_merge(
			config_array(APPLICATIONS),
			config_array(SYSTEM_APPLICATIONS));

		return in_array($application, $applications);
	}
?>
