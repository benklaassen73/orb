<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	class desktop {
		private $view = null;
		private $username = null;

		public function __construct($view, $username) {
			$this->view = $view;
			$this->username = $username;
		}

		/* Get request handler
		 *
		 * INPUT:  -
		 * OUTPUT: object request handler
		 * ERROR:  false
		 */
		private function get_request_handler() {
			list($request_uri, $parameters) = explode("?", $_SERVER["REQUEST_URI"], 2);
			$parts = explode("/", $request_uri);
			$name = $parts[1];

			if ($name == "") {
				return null;
			}

			if ($name == "orb") {
				$name = $parts[2];

				if ($name == "") {
					return false;
				}

				if (class_exists($name)) {
					if (is_subclass_of($name, "orb_backend")) {
						$systemcall = new $name($this->view, $this->username);
						return $systemcall;
					}
				}

				$systemcall = new orb($this->view, $this->username);
				return $systemcall;

				return false;
			}

			$sections = array(
				"apps"   => config_array(APPLICATIONS),
				"system" => SYSTEM_APPLICATIONS);

			$application = false;

			foreach ($sections as $directory => $applications) {
				if (in_array($name, $applications) == false) {
					continue;
				}

				$library = $directory."/".$name."/".$name.".php";

				if (file_exists($library)) {
					ob_start();
					require_once $library;
					ob_end_clean();
				}

				if (class_exists($name) == false) {
					continue;
				}

				if (is_subclass_of($name, "orb_backend") == false) {
					continue;
				}

				$application = new $name($this->view, $this->username);

				break;
			}

			return $application;
		}

		/* Show desktop
		 *
		 * INPUT:  -
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function show() {
			$core_parts = array("desktop", "windows", "taskbar", "file", "directory");

			/* Stylesheets
			 */
			$this->view->add_css("jquery-ui.css");
			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("context-menu.css");
			$this->view->add_css("font-awesome.css");
			foreach ($core_parts as $part) {
				$this->view->add_css($part.".css");
			}

			/* Javascripts
			 */
			$this->view->add_javascript("jquery.js");
			$this->view->add_javascript("jquery-ui.js");
			if ($this->view->mobile_device) {
				$this->view->add_javascript("jquery.ui.touch-punch.js");
			}
			$this->view->add_javascript("bootstrap.js");
			$this->view->add_javascript("jquery.contextMenu.js");
			$this->view->add_javascript("orb.js");
			$this->view->add_javascript("library.js");

			foreach (SYSTEM_APPLICATIONS as $application) {
				$this->view->add_system_application($application);
			}

			$applications = config_array(APPLICATIONS);

			foreach ($applications as $application) {
				$this->view->add_application($application);
			}

			foreach ($core_parts as $part) {
				$this->view->add_javascript($part.".js");
			}

			/* Login information
			 */
			$this->view->open_tag("login");
			$this->view->add_tag("method", AUTHENTICATION);
			if (AUTHENTICATION == "orb") {
				$this->view->add_tag("timeout", ini_get("session.gc_maxlifetime"));
			}
			$this->view->close_tag();

			/* Desktop icons
			 */
			if (substr(HOME_DIRECTORIES, 0, 1) == "/") {
				$homedir = HOME_DIRECTORIES;
			} else {
				$homedir = __DIR__."/../".HOME_DIRECTORIES;
			}
			$homedir .= "/".$this->username;
			$desktop = $homedir."/Desktop";

			if (is_dir($desktop) == false) {
				return false;
			}

			if (($dp = opendir($desktop)) == false) {
				return false;
			}

			$files = array();
			while (($file = readdir($dp)) != false) {
				if (substr($file, 0, 1) == ".") {
					continue;
				}

				array_push($files, $file);
			}
			sort($files);

			closedir($dp);

			/* Load settings
			 */
			ob_start();
			$settings = file_get_contents($homedir."/.settings");
			ob_end_clean();

			if ($settings !== false) {
				$settings = json_decode($settings, true);
			} else {
				$settings = array("system" => array("zoom" => 0.75));
			}

			/* Create desktop
			 */
			$this->view->open_tag("desktop", array(
				"mobile" => show_boolean($this->view->mobile_device),
				"zoom"   => $settings["system"]["zoom"]));

			$this->view->add_tag("taskbar");

			$this->view->open_tag("icons");

			foreach ($files as $file) {
				$type = is_dir($desktop."/".$file) ? "directory" : "file";
				$this->view->add_tag("icon", $file, array("type" => $type));
			}

			$this->view->close_tag();

			$this->view->close_tag();

			return true;
		}

		public function execute() {
			$request_handler = $this->get_request_handler();

			if ($request_handler === null) {
				/* Desktop
				 */
				$xslt_file = "desktop";

				if ($this->show() == false) {
					$this->view->return_error(500);
				}
			} else if ($request_handler === false) {
				/* Error
				 */
				ob_get_clean();

				header("Status: 404");
				print "File not found.";
			} else {
				/* Application backend requests
				 */
				$xslt_file = null;

				if (is_true(DEBUG_MODE) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
					debug_log($_POST);
				}

				$request_handler->execute();
			}

			return $xslt_file;
		}
	}
?>
