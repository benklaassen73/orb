#!/bin/bash

whiptail -v > /dev/null 2> /dev/null
if [ $? != 0 ]; then
	echo "This script requires whiptail."
	exit;
fi

# Initialize
#
cd `dirname $0`
swd=$PWD

version=`grep ORB_VERSION libraries/orb.php | cut -f4 -d'"'`
homedir=`grep HOME_DIRECTORIES orb.conf | cut -f2 -d= | sed 's/^ *//g'`
backtitle="Orb v${version} setup tool"

# Show message and wait for key
#
function press_any_key {
	read -n 1 -r -s -p $'\nPress enter to continue...\n'
}

# Create new user account
#
function create_user {
	username=""
	while [ "${username}" = "" ]; do
		username=$(whiptail \
			--backtitle "${backtitle}" \
			--inputbox "Username:" 8 60 \
			3>&1 1>&2 2>&3)
		
		if [ $? = 1 ]; then
			return
		fi
	done

	if [ "`grep ^${username}: ${homedir}/users.txt`" != "" ]; then
		echo "That user already has an account."
		press_any_key
		return
	fi

	password=""
	while [ "${password}" = "" ]; do
		password=$(whiptail \
			--backtitle "${backtitle}" \
			--passwordbox "Password:" 8 60 \
			3>&1 1>&2 2>&3)

		if [ $? = 1 ]; then
			return
		fi
	done

	password=`php -r "print password_hash(\"${password}\", PASSWORD_ARGON2I);"`

	echo "${username}:${password}" >> ${homedir}/users.txt

	echo "User account has been created."

	press_any_key
}

# Create new user home directory
#
function create_home_directory {
	username=""
	while [ "${username}" = "" ]; do
		username=$(whiptail \
			--backtitle "${backtitle}" \
			--inputbox "Orb username:" 8 60 \
			3>&1 1>&2 2>&3)
		
		if [ $? = 1 ]; then
			return
		fi
	done

	if [ ! -d ${homedir} ]; then
		mkdir ${homedir}
	fi

	cd ${homedir}

	if [ -d ${username} ]; then
		echo "That user already has a home directory."
		cd ${swd}
		press_any_key
		return
	fi

	mkdir ${username}
	cd ${username}
	mkdir Bookmarks
	mkdir Desktop
	mkdir Documents
	mkdir Pictures

	cp ../../public/images/wallpaper.jpg Pictures
	cp ../../extra/user.settings .settings
	cd ${swd}

	echo "Home directory ${homedir}/${username} and settings file ${homedir}/${username}/.settings created."
	echo "Make sure that they are writable for your webserver."


	press_any_key
}

# Create new Orb application from template
#
function create_application {
	app=""
	while [ "${app}" = "" ]; do
		app=$(whiptail \
			--backtitle "${backtitle}" \
			--inputbox "Application name:" 8 60 \
			3>&1 1>&2 2>&3)
		
		if [ $? = 1 ]; then
			return
		fi
	done

	app=`echo ${app} | tr '[:upper:]' '[:lower:]'`
	name=`echo ${app} | sed 's/./\u&/'`
	app=`echo ${app} | sed 's/ /_/g'`
	author=`grep "^${USER}:" /etc/passwd | cut -f5 -d: | cut -f1 -d,`

	if [ "${app}" = "orb" ]; then
		echo "That application name is not allowed."
		press_any_key
		exit
	fi

	if [ -d "public/apps/${app}" ]; then
		echo "Application ${app} already exists."
		press_any_key
		exit
	fi

	mkdir public/apps/${app}

	cat extra/template/template.js | \
		sed "s/template/${app}/g" | \
		sed "s/Template/${name}/g" | \
		sed "s/NAME/${author}/g" > \
		public/apps/${app}/${app}.js
	sed "s/template/${app}/g" < extra/template/template.css > public/apps/${app}/${app}.css
	sed "s/template/${app}/g" < extra/template/template.php > public/apps/${app}/${app}.php

	cat orb.conf | sed "s/^APPLICATIONS =.*/\0|${app}/" > orb.new
	mv orb.new orb.conf

	echo "New application available in public/apps/${app}."

	press_any_key
}

# Main menu
#
function main_menu {
	while [ true ]; do
		choice=$(whiptail \
			--backtitle "${backtitle}" \
			--title "Setup options" \
			--clear --notags \
			--cancel-button "Exit" \
			--menu "" 0 0 0 \
			"U" "Create user account" \
			"H" "Create user home directory" \
			"A" "Create new application" \
			3>&1 1>&2 2>&3)

		if [ $? = 1 ]; then
			break
		fi

		case ${choice} in
			"U") create_user;;
			"H") create_home_directory;;
			"A") create_application;;
		esac
	done
}

main_menu
