<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	ob_start();

	session_name("Orb");
	session_start();

	require "../libraries/error.php";
	require "../libraries/general.php";
	require "../libraries/xml.php";
	require "../libraries/orb.php";

	$view = new view();

	$view->open_tag("output", array(
		"version" => ORB_VERSION,
		"debug"   => show_boolean(DEBUG_MODE)));

	$login = new login($view);

	if ($login->valid() == false) {
		/* Authentication
		 */
		$xslt_file = $login->execute();
	} else {
		/* Desktop
		 */
		$desktop = new desktop($view, $login->username);

		$xslt_file = $desktop->execute();
	}

	$view->close_tag();

	/* Generate output
	 */
	if (($errors = ob_get_clean()) != "") {
		show_error($errors);
	} else if (($html = $view->generate($xslt_file)) == false) {
		header("Status: 500");
		show_error("XSLT error.");
	} else {
		print $html;
	}
?>
