/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var settings_windows_open = [];
var settings_default_color = null;
var settings_wallpaper_extensions = [ 'gif', 'jpeg', 'jpg', 'png', 'webp' ];

/* Colors
 */
function settings_color() {
	if (settings_windows_open.includes('color')) {
		return;
	}

	var window_content =
		'<div class="color">' +
		'<form><input type="text" id="color" name="color" value="' +
		settings_default_color + '" /></form>' +
		'<div id="colorpicker"></div>' +
		'<div class="btn-group">' +
		'<button class="btn btn-default save">Save</button>' +
		'<button class="btn btn-default default">Default</button>' +
		'</div>' +
		'</div>';

	var settings_color_window = $(window_content).orb_window({
		header:'Color',
		icon: '/system/settings/color.png',
		width: 220,
		height: 270,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'color');
		}
	});

	orb_load_javascript('/system/settings/color/farbtastic.js');
	orb_load_stylesheet('/system/settings/color/farbtastic.css');
	var farbtastic = settings_color_window.find('#colorpicker').farbtastic('input#color');

	orb_setting_get('system/color', function(color) {
		jQuery.farbtastic('#colorpicker').setColor(color);
	}, function() {
		alert('Error loading color.');
	});

	settings_color_window.find('button.save').click(function() {
		var color = settings_color_window.find('input#color').val();
		orb_setting_set('system/color', color, function() {
			orb_window_set_color(color);
			settings_color_window.close();
		}, function() {
			alert('Error setting color.');
		});
	});

	settings_color_window.find('button.default').click(function() {
		jQuery.farbtastic('#colorpicker').setColor(settings_default_color);
	});

	settings_color_window.open();

	settings_windows_open.push('color');
}

/* Wallpaper
 */
function settings_wallpaper() {
	if (settings_windows_open.includes('wallpaper')) {
		return;
	}

	var window_content =
		'<div class="wallpaper">' +
		'<label>Wallpaper:</label>' +
		'<div class="input-group">' +
		'<input type="text" readonly="readonly" class="form-control">' +
		'<span class="input-group-btn">' +
		'<button class="btn btn-default" type="button">Select wallpaper</button>' +
		'</span>' +
		'</div>' +
		'</div>';

	var settings_wallpaper_window = $(window_content).orb_window({
		header:'Wallpaper',
		icon: '/system/settings/wallpaper.png',
		width: 500,
		height: 100,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'wallpaper');
		}
	});

	orb_setting_get('system/wallpaper', function(wallpaper) {
		settings_wallpaper_window.find('input').val(wallpaper);
	}, function() {
		alert('Error loading wallpaper.');
	});

	settings_wallpaper_window.find('button').click(function() {
		orb_file_dialog('Select', function(wallpaper) {
			var extension = orb_file_extension(wallpaper);

			if (settings_wallpaper_extensions.includes(extension) == false) {
				alert('Invalid wallpaper file.');
			} else {
				orb_setting_set('system/wallpaper', wallpaper, function() {
					settings_wallpaper_window.find('input').val(wallpaper);
					orb_desktop_load_wallpaper(wallpaper);
				}, function() {
					alert('Error setting wallpaper.');
				});
			}
		}, 'Pictures');
	});

	settings_wallpaper_window.open();

	settings_windows_open.push('wallpaper');
}

/* Zoom for mobile devices
 */
function settings_zoom() {
	if (settings_windows_open.includes('zoom')) {
		return;
	}

	var window_content =
		'<div class="zoom">' +
		'<label>Zoom for mobile devices:</label>' +
		'<div id="slider"></div>' +
		'<div class="btn-group">' +
		'<button class="btn btn-default" type="button">Set zoom</button>' +
		'</div>' +
		'</div>';

	var settings_zoom_window = $(window_content).orb_window({
		header:'Mobile zoom',
		icon: '/system/settings/zoom.png',
		width: 500,
		height: 100,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'zoom');
		}
	});

	settings_zoom_window.find('div#slider').slider({
		min: 0.5,
		max: 1,
		step: 0.05,
		create: function() {
			settings_zoom_window.find('div#slider span').text($(this).slider('value'));
		},
		slide: function(event, ui) {
			settings_zoom_window.find('div#slider span').text(ui.value);
		},
	});

	orb_setting_get('system/zoom', function(zoom) {
		settings_zoom_window.find('div#slider').slider('option', 'value', zoom);
		settings_zoom_window.find('div#slider span').text(zoom);
	}, function() {
		alert('Error loading mobile zoom factor.');
	});

	settings_zoom_window.find('button').click(function() {
		var zoom = settings_zoom_window.find('div#slider').slider('value');
		orb_setting_set('system/zoom', zoom, function() {
			settings_zoom_window.close();
			if ($('div.desktop').attr('mobile') == 'yes') {
				var content = 'width=device-width, initial-scale=' + zoom + ', maximum-scale=' + zoom
				$('head meta[name=viewport]').attr('content', content);
			}
		}, function() {
			alert('Error setting mobile zoom factor.');
		});
	});

	settings_zoom_window.open();

	settings_windows_open.push('zoom');
}


/* Settings
 */
function settings_add_section(settings_window, icon_label, icon_image, callback) {
	var icon = orb_make_icon(icon_label, '/system/settings/' + icon_image);
	var section = $(icon);
	section.find('img').attr('draggable', 'false');
	section.dblclick(callback);

	settings_window.append(section);
}


function settings_open(filename = undefined) {
	if (settings_windows_open.includes('settings')) {
		return;
	}

	$.ajax({
		url: '/settings/color'
	}).done(function(data) {
		settings_default_color = $(data).find('color').text();
	});

	var window_content =
		'<div class="settings">' +
		'</div>';

	var settings_window = $(window_content).orb_window({
		header:'Settings',
		icon: '/system/settings/settings.png',
		width: 400,
		height: 200,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'settings');
		}
	});

	settings_add_section(settings_window, 'Color', 'color.png', settings_color);
	settings_add_section(settings_window, 'Mobile zoom', 'zoom.png', settings_zoom);
	settings_add_section(settings_window, 'Wallpaper', 'wallpaper.png', settings_wallpaper);

	settings_window.open();

	settings_windows_open.push('settings');
}

$(document).ready(function() {
	orb_startmenu_system('Settings', '/system/settings/settings.png', settings_open);
});
