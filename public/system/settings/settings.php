<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class settings extends orb_backend {
		public function get_color() {
			$this->view->add_tag("color", DEFAULT_COLOR);
		}
	}
?>
