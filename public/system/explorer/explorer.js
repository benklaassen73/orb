/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function explorer_set_path(explorer_window, path) {
	explorer_window.find('form input[name=path]').val(path);

	var target = explorer_window.find('div.path');

	target.empty();
	target.append('<a href="">/</a>');

	if (path != '') {
		var parts = path.split('/');
		var url = [];
		parts.forEach(function(part) {
			url.push(part);
			part += '/';
			target.append('<a href="' + url.join('/') + '">' + part + '</a>');
		});
	}

	target.find('a').click(function() {
		explorer_window.data('path', $(this).attr('href'));
		explorer_update(explorer_window);
		return false;
	});
}

function explorer_contextmenu_handler(key, options) {
	var explorer_window = $(this).parent().parent();
	var path = explorer_window.data('path');
	var filename = $(this).find('span').first().text();

	switch (key) {
		case 'download':
			var url = '/orb/file/download/' + url_encode(path);
			if (path != '') {
				url += '/';
			}
			url += url_encode(filename);

			window.open(url, '_blank').focus();
			break;
		case 'edit':
			notepad_open(path + '/' + filename);
			break;
		case 'rename':
			var new_filename = prompt('Rename file', filename);
			if (new_filename !== null) {
				orb_file_rename(path + '/' + filename, new_filename, undefined, function() {
					alert('Error while renaming file or directory.');
				});
			}
			break;
		case 'delete':
			if (confirm('Delete ' + filename + '?')) {
				orb_file_remove(path + '/' + filename, undefined, function() {
					alert('Error while deleting file.');
				});
			}
			break;
		case 'rmdir':
			if (confirm('Delete ' + filename + '?')) {
				orb_directory_remove(path + '/' + filename, undefined, function() {
					alert('Error while deleting directory.');
				});
			}
			break;
	}
}

/* File icon
 */
function explorer_entry_add(files, format, item, path) {
	if (item.type == 'directory') {
		var image = '/images/directory.png';
	} else {
		var extension = orb_file_extension(item.name);
		if (extension != false) {
			var image = orb_get_file_icon(extension);
		} else {
			var image = '/images/file.png';
			extension = '';
		}
	}

	var icon = '<div class="entry ' + format + '" type="' + item.type + '" ext="' + extension + '">' +
		'<img src="' + image + '" alt="' + item.name + '" title="' + item.name + '" />' +
		'<span path="' + path + '" type="' + item.type + '"'
	if (item.create != undefined) {
		icon += ' create="' + item.create + '"';
	}
	if (item.access != undefined) {
		icon += ' access="' + item.access + '"';
	}
	icon += '>' + item.name + '</span>';
	if ((item.type == 'file') && (item.size != undefined)) {
		icon += '<span number="' + item.size + '">';
		icon += orb_file_nice_size(item.size);
	} else {
		icon += '<span>&nbsp;';
	}

	if ($('div.desktop').attr('mobile') == 'yes') {
		icon += '</span><span class="glyphicon glyphicon-chevron-down" aria-hidden="true">';
	}

	icon += '</span></div>';

	files.append(icon);
}

function explorer_update(explorer_window) {
	var path = explorer_window.data('path');

	explorer_set_path(explorer_window, path);

	orb_directory_list(path, function(items) {
		var information = explorer_window.find('div.information');
		var files = explorer_window.find('div.files');
		var filename = explorer_window.find('input.filename');
		var format = explorer_window.data('format');

		information.empty();
		files.empty();
		filename.val('');

		/* Fill explorer
		 */
		items.forEach(function(item) {
			if (item.type == 'directory') {
				explorer_entry_add(files, format, item, path);
			}
		});

		items.forEach(function(item) {
			if (item.type == 'file') {
				explorer_entry_add(files, format, item, path);
			}
		});

		/* Mobile
		 */
		if ($('div.desktop').attr('mobile') == 'yes') {
			explorer_window.find('div.files div.detail span:nth-of-type(2)').css('right', '25px');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').css('display', 'inline');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').click(function(event) {
				$(this).parent().trigger('contextmenu');
				event.stopPropagation();
			});
		}

		/* Double-click file
		 */
		explorer_window.find('div.files div.entry[type=directory]').dblclick(function() {
			var dir = $(this).find('span').first().text();

			if (path == '') {
				path = dir;
			} else {
				path += '/' + dir;
			}

			explorer_window.data('path', path);
			explorer_update(explorer_window);
		});

		/* Double-click file
		 */
		explorer_window.find('div.files div.entry[type=file]').dblclick(function() {
			var filename = path + '/' + $(this).find('span').first().text();

			var parts = filename.split('.');
			if (parts.length == 1) {
				return;
			}

			var extension = parts.pop();
			if (extension == '') {
				return;
			}

			if ((handler = orb_get_file_handler(extension)) != undefined) {
				handler(filename);
			} else {
				window.open('/orb/file/download/' + url_encode(filename), '_blank').focus();
			}
		});

		/* Click file
		 */
		explorer_window.find('div.files div.entry').click(function() {
			var name = $(this).find('span').first().text();

			if ($('div.desktop').attr('mobile') == 'yes') {
				if (explorer_window.data('click_last') == name) {
					explorer_window.data('click_last', null);
					$(this).trigger('dblclick');
				} else {
					explorer_window.data('click_last', name);
				}
			}

			explorer_window.find('div.files div.selected').removeClass('selected');
			$(this).addClass('selected');

			var information = explorer_window.find('div.information');
			information.empty();

			var src = $(this).find('img').attr('src');
			information.append('<img src="' + src + '" />');

			information.append('<p><b>File name:</b><br />' + name + '</p>');

			var create = $(this).find('span').first().attr('create');
			if ((create != undefined) && (create != '')) {
				var parts = create.split(', ');
				information.append('<p><b>Creation date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var access = $(this).find('span').first().attr('access');
			if ((access != undefined) && (access != '')) {
				var parts = access.split(', ');
				information.append('<p><b>Last access date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var type = $(this).find('span').first().attr('type');
			if (type == 'file') {
				var size = $(this).find('span').last().attr('number');
				if (size != null) {
					information.append('<p><b>File size:</b><br />' + size + '</p>');
				}
			}
		});

		/* Right-click file
		 */
		var window_id = explorer_window.parent().parent().attr('id');

		$.contextMenu({
			selector: 'div#' + window_id + ' div.files div.entry[type=directory]',
			callback: explorer_contextmenu_handler,
			items: {
				'rename': { name: 'Rename', icon: 'edit' },
				'rmdir': { name: 'Delete', icon: 'delete' },
			}
		});

		$.contextMenu({
			selector: 'div#' + window_id + ' div.files div.entry[type=file][ext=ots]',
			callback: explorer_contextmenu_handler,
			items: {
				'edit': { name: 'Edit', icon: 'fa-file-text-o' },
				'rename': { name: 'Rename', icon: 'edit' },
				'delete': { name: 'Delete', icon: 'delete' },
			}
		});

		$.contextMenu({
			selector: 'div#' + window_id + ' div.files div.entry[type=file]',
			callback: explorer_contextmenu_handler,
			items: {
				'download': { name: 'Download', icon: 'fa-download' },
				'rename': { name: 'Rename', icon: 'edit' },
				'delete': { name: 'Delete', icon: 'delete' },
			}
		});

		explorer_window.find('div.files > div.entry').draggable({
			appendTo: 'div.icons',
			helper: 'clone',
			zIndex: 10000,
			start: function(event, ui) {
				if (ui.helper.hasClass('detail')) {
					ui.helper.removeClass('detail');
					ui.helper.addClass('icon');
				}
			}
		});

		if (($('div.desktop').attr('mobile') == 'yes') && ($('input.drag').prop('checked') == false)) {
			explorer_window.find('div.files > div.entry').draggable({
				disabled: true
			});
		}
	}, function(result) {
		if (result == 401) {
			explorer_window.close();
			alert('Login has been expired. No access to disk.');
			orb_logout();
		} else if (path != '') {
			alert('Error reading directory.');
			explorer_window.data('path', '');
			explorer_update(explorer_window);
		}
	});
}

function explorer_set_format(explorer_window, format) {
	if (explorer_window.data('format') != format) {
		explorer_window.data('format', format);
		orb_setting_set('applications/explorer/format', format);
		explorer_update(explorer_window);
	}
}

function explorer_menu_click(explorer_window, item) {
	explorer_window = explorer_window.find('div.explorer');

	switch (item) {
		case 'New Explorer window':
			var path = explorer_window.data('path');
			explorer_open(path);
			break;
		case 'New directory':
			var path = explorer_window.data('path');
			var directory = prompt('New directory name');

			if (directory == null) {
				break;
			}

			if (directory.indexOf('/') != -1) {
				break;
			}

			if ((directory != null) && (directory.trim() != '')) {
				orb_directory_create(path + '/' + directory, undefined, function() {
					alert('Error while creating directory.');
				});
				break;
			}
			break;
		case 'Icons':
			explorer_set_format(explorer_window, 'icon');
			break;
		case 'Details':
			explorer_set_format(explorer_window, 'detail');
			break;
		case 'Exit':
			explorer_window.close();
			break;
		case 'About':
			alert('File explorer\nCopyright (c) by Hugo Leisink');
			break;
	}
}

function explorer_open(directory = undefined) {
	var dropzone_id = 0
	while ($('div.explorer form#dropzone' + dropzone_id).length > 0) {
		dropzone_id++;
	}

	var window_content =
		'<div class="explorer">' +
		'<button class="btn btn-default btn-xs directory_up"><span class="fa fa-chevron-up"></span></button>' +
		'<div class="path"></div>' +
		'<div class="information"></div>' +
		'<div class="files"></div>' +
		'<form action="/explorer" class="dropzone" id="dropzone' + dropzone_id + '">' +
		'<div class="dz-message" data-dz-message><span>Drop files here to upload them to the current folder.</span></div>' +
		'<input type="hidden" name="path" value="" />' +
		'</form>' +
		'</div>';

	var explorer_window = $(window_content).orb_window({
		header:'Explorer',
		icon: '/system/explorer/explorer.png',
		width: 760,
		height: 430,
		menu: {
			'File': [ 'New Explorer window', 'New directory', '-', 'Exit' ],
			'View': [ 'Icons', 'Details' ],
			'Help': [ 'About' ]
		},
		menuCallback: explorer_menu_click
	});

	explorer_window.parent().parent().css('min-height', '270px');

	explorer_window.open();

	if (directory == undefined) {
		directory = '';
	}

	explorer_window.data('path', directory);

	orb_setting_get('applications/explorer/format', function(value) {
		explorer_window.data('format', value);
		explorer_update(explorer_window);
	}, function() {
		explorer_window.data('format', 'icon');
		explorer_update(explorer_window);
		orb_setting_set('applications/explorer/format', 'icon');
	});

	explorer_window.find('button.directory_up').click(function() {
		var path = explorer_window.data('path');

		if (path == '') {
			return;
		}

		var parts = path.split('/');
		parts.pop();
		path = parts.join('/');

		explorer_window.data('path', path);
		explorer_update(explorer_window);
	});

	explorer_window.droppable({
		accept: 'div.icon, div.detail',
		greedy: true,
		over: function() {
			var current = $(this);
			$('div.windows div.explorer').each(function() {
				if ($(this).is(current) == false) {
					$(this).droppable('disable');
				}
			});

			orb_window_raise(explorer_window.parent().parent());
		},
		out: function() {
			$('div.windows div.explorer').droppable('enable');
		},
		drop: function(event, ui) {
			event.stopPropagation();

			var span = ui.helper.find('span').first();
			var source = span.attr('path') + '/' + span.text();
			var destination_path = explorer_window.data('path');

			if (orb_key_pressed(KEY_CTRL)) {
				orb_file_copy(source, destination_path, undefined, function() {
					alert('Error copying file.');
				});
			} else {
				orb_file_move(source, destination_path, undefined, function() {
					alert('Error moving file.');
				});
			}

			$('div.windows div.explorer').droppable('enable');
		}
	});

	/* Dropzone
	 */
	var dropzone_visible = false;
	var file_dropped = false;

	Dropzone.options['dropzone' + dropzone_id] = {
		init: function() {
			this.on('success', function(file) {
				file_dropped = true;
				var path = explorer_window.data('path');
				orb_directory_notify_update(path);
			});

			this.on('complete', function(file) {
				var dropzone = this;
				setTimeout(function() {
					dropzone.removeFile(file)
					explorer_window.find('form.dropzone').hide();
					dropzone_visible = false;
					file_dropped= false;
				}, 3000);
			});
		}
	};

	var dropzone = new Dropzone('#dropzone' + dropzone_id, {
		url: '/explorer'
	});

	explorer_window.on('dragover', function(event) {
		event.preventDefault();
		event.stopPropagation();

		if (dropzone_visible == false) {
			dropzone_visible = true;
			explorer_window.find('form.dropzone').show();

			setTimeout(function() {
				if (file_dropped == false) {
					explorer_window.find('form.dropzone').hide();
					dropzone_visible = false;
				}
			}, 3000);
		}
	});

	/* Mobile support
	 */
	if ($('div.desktop').attr('mobile') == 'yes') {
		explorer_window.find('div.information').css('width', '150px');
		explorer_window.find('div.information').css('font-size', '12px');
		explorer_window.find('div.files').css('left', '165px');

		explorer_window.find('form.dropzone').css('bottom', '30px');

		explorer_window.find('div.information').css('bottom', '25px');
		var drag = $('<div class="drag"><input type="checkbox" class="drag" />Icons draggable</div>');
		drag.find('input').click(function() {
			var checked = $(this).prop('checked');
			explorer_window.find('div.files > div.entry').draggable({
				disabled: checked == false
			});
		});
		explorer_window.append(drag);

		explorer_window.find('div.files').css('bottom', '30px');
		var scroll = $('<div class="scroll btn-group"><button class="btn btn-default btn-xs scroll_up"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></button><button class="btn btn-default btn-xs scroll_down"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>');
		scroll.find('button.scroll_up').click(function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current - 88);
		});
		scroll.find('button.scroll_down').click(function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current + 88);
		});
		explorer_window.append(scroll);
	}
}

$(document).ready(function() {
	orb_startmenu_add('Explorer', '/system/explorer/explorer.png', explorer_open);
	orb_upon_directory_open(explorer_open);

	orb_directory_upon_update(function(directory) {
		$('div.windows div.explorer').each(function() {
			if ($(this).data('path') == directory) {
				explorer_update($(this));
			}
		});
	});

	orb_load_javascript('/system/explorer/dropzone.js');
	orb_load_stylesheet('/system/explorer/dropzone.css');
});
