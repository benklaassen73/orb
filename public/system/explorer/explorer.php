<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class explorer extends orb_backend {
		public function post() {
			if (is_true(DEBUG_MODE)) {
				debug_log($_FILES);
			}

			if (is_array($_FILES["file"]) == false) {
				$this->view->return_error(400);
				return;
			}

			if ($_FILES["file"]["error"] != 0) {
				$this->view->return_error(400);
				return;
			}

			$path = trim($_POST["path"], "/ ");
			if ($this->valid_filename($path) == false) {
				$this->view->return_error(400);
				return;
			}

			$destination = $this->home_directory."/".$path;

			if (is_dir($destination) == false) {
				$this->view->return_error(404);
				return;
			}

			if ($path != "" ) {
				$destination .= "/";
			}
			$destination .= $_FILES["file"]["name"];

			move_uploaded_file($_FILES["file"]["tmp_name"], $destination);

			$this->view->add_tag("path", $path);
		}
	}
?>
