const KEY_SHIFT = 16;
const KEY_CTRL = 17;

var _orb_setting_error_shown = false;
var _orb_file_icons = [];
var _orb_callbacks_open_file = {};
var _orb_callback_open_directory = undefined;
var _orb_keys_down = {}
_orb_keys_down[KEY_SHIFT] = false;
_orb_keys_down[KEY_CTRL] = false;

/* Create icon
 */
function orb_make_icon(name, image) {
	return '<div class="icon">' +
		'<img src="' + image + '" alt="' + name + '" title="' + name + '" />' +
		'<span>' + name + '</span></div>';
}

/* File icons
 */
function orb_get_file_icon(extension) {
	var default_icon = '/images/file.png';
	var handler = _orb_callbacks_open_file[extension];

	if (_orb_file_icons.includes(extension)) {
		default_icon = '/images/icons/' + extension + '.png';
	}

	if (handler == undefined) {
		return default_icon;
	}

	if (handler.icon == undefined) {
		return default_icon;
	}

	return handler.icon;
}

/* File and directory handlers
 */
function orb_upon_file_open(extension, callback, icon = undefined) {
	var handler = {
		callback: callback,
		icon: icon
	}

	if (_orb_callbacks_open_file[extension] == undefined) {
		_orb_callbacks_open_file[extension] = handler;
	}
}

function orb_upon_directory_open(callback) {
	if (_orb_callback_open_directory == undefined) {
		_orb_callback_open_directory = callback;
	}
}

function orb_get_file_handler(extension) {
	var handler = _orb_callbacks_open_file[extension];

	if (handler == undefined) {
		return undefined;
	}

	return handler.callback;
}

function orb_get_directory_handler(extension) {
	return _orb_callback_open_directory;
}

/* Cookie
 */
function orb_get_cookie(cookie) {
	var parts = document.cookie.split(';');

	var cookies = {};
	parts.forEach(function(part) {
		var item = part.split('=');
		var key = item[0].trim();
		var value = item[1].trim();

		cookies[key] = value;
	});

	return cookies[cookie];
}

/* Settings
 */
function orb_setting_get(setting, callback_done, callback_fail = undefined) {
	$.ajax({
		url: '/orb/setting/' + setting
	}).done(function(data) {
		var result = $(data).find('result').text();
		callback_done(result);
	}).fail(function(result) {
		if ((result.status == 500) && (_orb_setting_error_shown == false)) {
			_orb_setting_error_shown = true;
			alert('User settings file not found. Read INSTALL for instructions.');
		}

		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

function orb_setting_set(setting, value, callback_done = undefined, callback_fail = undefined) {
	$.post('/orb/setting/' + setting, {
		value: value
	}).done(function() {
		if (callback_done != undefined) {
			callback_done();
		}
	}).fail(function(result) {
		if ((result.status == 500) && (_orb_setting_error_shown == false)) {
			_orb_setting_error_shown = true;
			alert('User settings file not writable for webserver.');
		}

		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

/* Dynamically add resources
 */
function orb_load_javascript(javascript) {
	if ($('head script[src="' + javascript + '"]').length > 0) {
		return;
	}

	$('head').append('<script type="text/javascript" src="' + javascript + '"></script>');
}

function orb_load_stylesheet(stylesheet) {
	if ($('head link[href="' + stylesheet + '"]').length > 0) {
		return;
	}

	$('head').append('<link rel="stylesheet" type="text/css" href="' + stylesheet + '" />');
}

/* Logout
 */
function _orb_session_timeout() {
	$.ajax({
		url: '/orb/ping'
	}).done(function() {
		setTimeout(_orb_session_timeout, 60000);
	}).fail(function(result) {
		if (result.status == 401) {
			orb_logout();
		}
	});
}

function orb_logout() {
	if ($('div.windows div.window').length > 0) {
		if (confirm('Close all windows and logout?') == false) {
			return;
		}
	}

	var login = $('div.desktop').attr('login');

	var logout = window.location.protocol + '//';

	if (login == 'http') {
		logout += 'log:out@';
	}

	logout += window.location.hostname;

	if (login == 'orb') {
		logout += '/?logout';
	}

	$('body').empty().css('background-color', '#202020');
	window.location = logout;
}

/* Key press
 */
function orb_key_pressed(key) {
	return _orb_keys_down[key];
}

/* Main
 */
$(document).ready(function() {
	$.ajax({
		url: '/orb/icon/default'
	}).done(function(data) {
		$(data).find('icon').each(function() {
			_orb_file_icons.push($(this).text());
		});
	}).fail(function() {
		alert('Error loading custom icons.');
	});

	/* Register ctrl press
	 */
	$('body').keydown(function(event) {
		if (_orb_keys_down[event.which] !== undefined) {
			_orb_keys_down[event.which] = true;
		}
	});

	$('body').keyup(function(event) {
		if (_orb_keys_down[event.which] !== undefined) {
			_orb_keys_down[event.which] = false;
		}
	});

	var timeout = $('div.desktop').attr('timeout');
	if (timeout != undefined) {
		setTimeout(_orb_session_timeout, timeout * 1000);
	}
});
