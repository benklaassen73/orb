/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

/* Start menu
 */
function orb_startmenu_add(label, icon, callback) {
	var entry = $('<div class="application"><img src="' + icon + '" class="icon" draggable="false" /><span>' + label + '</span></div>');

	entry.click(orb_startmenu_close);
	entry.click(function() {
		callback();
	});

	var startmenu = $('div.taskbar div.startmenu div.applications');

	var applications = startmenu.find('div.application');
	if (applications.length == 0) {
		startmenu.append(entry);
		return;
	}

	var first = applications.first().find('span').text();
	if (label < first ) {
		startmenu.prepend(entry);
		return;
	}

	var added = false;
	applications.each(function() {
		var name = $(this).find('span').text();
		if (label < name) {
			$(this).before(entry);
			added = true;
			return false;
		}
	});

	if (added == false) {
		startmenu.append(entry);
	}
}

function orb_startmenu_system(label, icon, callback) {
	var entry = $('<img src="' + icon + '" class="icon" alt="' + label + '" title="' + label + '" draggable="false" />');

	entry.click(orb_startmenu_close);
	entry.click(function() {
		callback();
	});

	$('div.taskbar div.startmenu div.system').append(entry);
}

function orb_startmenu_close() {
	$('div.taskbar div.startmenu').hide();
}

/* Taskbar
 */
function orb_taskbar_add(task_id) {
	var task = $('div.windows div#' + task_id);
	var title = task.find('div.window-header').text();
	$('div.taskbar div.tasks').append('<div class="task" taskid="' + task_id + '">' + title + '</div>');

	$('div.taskbar div.tasks div.task[taskid=' + task_id + ']').click(function() {
		if ($(this).hasClass('minimized')) {
			$(this).removeClass('minimized');
			task.show();
			orb_window_raise(task);
		} else if (task.find('span.fa-window-minimize').length == 0) {
			orb_window_raise(task);
		} else if (task.hasClass('focus') == false) {
			orb_window_raise(task);
		} else {
			$(this).addClass('minimized');
			task.hide();
		}
	});
}

function orb_taskbar_focus(task_id) {
	$('div.taskbar div.tasks div.task').removeClass('focus');
	$('div.taskbar div.tasks div.task[taskid=' + task_id + ']').addClass('focus');
}

function orb_taskbar_remove(task_id) {
	$('div.taskbar div.tasks div.task[taskid=' + task_id + ']').remove();
}

/* Main
 */
$(document).ready(function() {
	$('div.taskbar div.start').click(function(event) {
		var zindex = orb_window_max_zindex() + 1;
		$('div.taskbar').css('z-index', zindex);
		$('div.taskbar div.startmenu').css('z-index', zindex + 1);
		$('div.taskbar div.startmenu').toggle(200);
		$('div.taskbar div.startmenu div.applications')[0].scrollTop = 0;
		event.stopPropagation();
	});

	$('div.taskbar').click(function(event) {
		event.stopPropagation();
	});
		
	$('div.taskbar div.startmenu').click(function(event) {
		event.stopPropagation();
	});

	orb_startmenu_system('Logout', '/images/logout.png', orb_logout);
});
