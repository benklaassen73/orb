/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

/* Icon
 */
function orb_icon_coord_to_grid(coord, grid_size) {
	var delta = coord % grid_size;
	coord -= delta;

	if (delta > (grid_size >> 1)) {
		coord += grid_size;
	}

	return coord;
}

/* Refresh desktop
 */
function orb_desktop_refresh() {
	orb_directory_list('Desktop', function(items) {
		var desktop = $('div.desktop div.icons');

		desktop.empty();

		/* Fill explorer
		 */
		items.forEach(function(item) {
			if (item.type == 'directory') {
				var icon = orb_file_make_icon(item.name, 'Desktop', 'directory');
				desktop.append(icon);
			}
		});

		items.forEach(function(item) {
			if (item.type == 'file') {
				var icon = orb_file_make_icon(item.name, 'Desktop', 'file');
				desktop.append(icon);
			}
		});

		var y = 0;
		$('div.desktop div.icons div.icon').each(function() {
			var width = Math.round($(this).outerWidth());
			var height = Math.round($(this).outerHeight());

			$(this).css('top', (y++ * height) + 'px');

			$(this).draggable({
				containment: 'parent',
				helper: 'clone',
				handle: 'img',
				zIndex: 10000,
				start: function() {
					orb_startmenu_close();
				},
				stop: function(event, ui) {
					if (orb_key_pressed(KEY_CTRL)) {
						return;
					}

					var pos = $(ui.helper).position();

					pos.top = orb_icon_coord_to_grid(pos.top, height);
					pos.left = orb_icon_coord_to_grid(pos.left, width);

					var win_y = $('div.icons').height();
					var icon = $(this);

					$('div.icons div.icon').each(function() {
						if ($(this).is(icon)) {
							return true;
						}

						var other = $(this).position();
						if ((pos.top == other.top) && (pos.left == other.left)) {
							pos.top += height;
						}
						if (pos.top + height > win_y) {
							pos.top -= height;
							pos.left += width;
						}
					});

					$(this).css('top', pos.top + 'px');
					$(this).css('left', pos.left + 'px');
				}
			});
		});

		$('div.desktop div.icons div.icon').dblclick(function() {
			var filename = 'Desktop/' + $(this).find('span').text();
			var type = $(this).attr('type');

			if (type == 'file') {
				var parts = filename.split('.');
				if (parts.length == 1) {
					return;
				}

				var extension = parts.pop();
				if (extension == '') {
					return;
				}

				if ((handler = orb_get_file_handler(extension)) != undefined) {
					handler(filename);
				} else {
					window.open('/orb/file/download/' + url_encode(filename), '_blank').focus();
				}
			} else {
				if ((handler = orb_get_directory_handler()) != undefined) {
					handler(filename);
				}
			}
		});
	}, function(result) {
		$('body').append('<h1>Error starting Orb</h1></p>The directory "Desktop" is missing in your home directory.</p>');
	});
}

/* Rearrange windows and icons on the desktop
 */
function orb_desktop_rearrange() {
	/* Rearrange windows
	 */
	var windows_width = Math.round($('div.windows').width());
	var windows_height = Math.round($('div.windows').height());

	$('div.windows div.window').each(function() {
		var pos = $(this).position();
		var width = Math.round($(this).outerWidth());
		var height = Math.round($(this).outerHeight());

		if (pos.left + width > windows_width) {
			pos.left = windows_width - width;
		}

		if (pos.left < 0) {
			pos.left = 0;
			if ($(this).is('.ui-resizable')) {
				if (pos.left + width > windows_width) {
					$(this).css('width', windows_width + 'px');
				}
			}
		}

		if (pos.top + height > windows_height) {
			pos.top = windows_height - height;
		}

		if (pos.top < 0) {
			pos.top = 0;
			if ($(this).is('.ui-resizable')) {
				if (pos.top + height > windows_height) {
					$(this).css('height', windows_height + 'px');
				}
			}
		}

		$(this).css('top', pos.top + 'px');
		$(this).css('left', pos.left + 'px');
	});

	/* Rearrange icons
	 */
	var icons_width = Math.round($('div.icons').width());
	var icons_height = Math.round($('div.icons').height());

	$('div.icons div.icon').each(function() {
		var pos = $(this).position();
		var width = Math.round($(this).outerWidth());
		var height = Math.round($(this).outerHeight());

		while (pos.left + width >= icons_width) {
			pos.left -= width;
			if (pos.left < 0) {
				pos.left = 0;
				break;
			}
		}
		
		while (pos.top + height >= icons_height) {
			pos.top -= height;
			if (pos.top < 0) {
				pos.top = 0;
				break;
			}
		}

		$(this).css('top', pos.top + 'px');
		$(this).css('left', pos.left + 'px');
	});
}

/* Load wallpaper
 */
function orb_desktop_load_wallpaper(wallpaper) {
	if (wallpaper.substring(0, 1) == '/') {
		wallpaper = wallpaper.substring(1);
	}

	$('div.desktop').css('background-image', 'url(/orb/file/download/' + url_encode(wallpaper) + ')');
}

/* Menu handler
 */
function orb_desktop_contextmenu_handler(key, options) {
	var filename = $(this).find('span').text();

	switch (key) {
		case 'refresh':
			orb_setting_get('system/wallpaper', function(wallpaper) {
				orb_desktop_load_wallpaper(wallpaper);
			});
			orb_desktop_refresh();
			break;
		case 'download':
			var url = '/orb/file/download/Desktop/' + url_encode(filename);
			window.open(url, '_blank').focus();
			break;
		case 'rename':
			var new_filename = prompt('Rename file', filename);
			if (new_filename !== null) {
				orb_file_rename('Desktop/' + filename, new_filename, undefined, function() {
					alert('Error while renaming file or directory.');
				});
			}
			break;
		case 'delete':
			if (confirm('Delete ' + filename + '?')) {
				orb_file_remove('Desktop/' + filename, undefined, function() {
					alert('Error while deleting file.');
				});
			}
			break;
		case 'rmdir':
			if (confirm('Delete ' + filename + '?')) {
				orb_directory_remove('Desktop/' + filename, undefined, function() {
					alert('Error while deleting directory.');
				});
			}
			break;
	}
}

/* Main
 */
$(document).ready(function() {
	orb_setting_get('system/wallpaper', function(wallpaper) {
		orb_desktop_load_wallpaper(wallpaper);
	}, function() {
		alert('Error loading wallpaper.');
	});

	orb_setting_get('system/color', function(color) {
		orb_window_set_color(color);
	}, function() {
		alert('Error loading window color.');
		orb_window_set_color('#808080');
	});

	orb_desktop_refresh();

	/* Droppable
	 */
	$('div.desktop').droppable({
		accept: 'div.icon, div.detail',
		drop: function(event, ui) {
			var span = ui.helper.find('span').first();
			var source_path = span.attr('path');
			var source = source_path + '/' + span.text();

			if (orb_key_pressed(KEY_CTRL)) {
				orb_file_copy(source, 'Desktop', undefined, function() {
					alert('Error copying file.');
				});
			} else {
				orb_file_move(source, 'Desktop', undefined, function() {
					alert('Error moving file.');
				});
			}
		}
	});

	/* Left click
	 */
	$('div.desktop').click(function() {
		orb_startmenu_close();
	});

	/* Right click
	$.contextMenu({
		selector: 'div.desktop',
		callback: orb_desktop_contextmenu_handler,
		items: {
			'refresh': { name: 'Refresh', icon: 'fa-refresh' }
		}
	});
	 */

	$.contextMenu({
		selector: 'div.desktop div.icons div.icon[type=directory]',
		callback: orb_desktop_contextmenu_handler,
		items: {
			'rename': { name: 'Rename', icon: 'edit' },
			'rmdir': { name: 'Delete', icon: 'delete' },
		}
	});

	$.contextMenu({
		selector: 'div.desktop div.icons div.icon[type=file]',
		callback: orb_desktop_contextmenu_handler,
		items: {
			'download': { name: 'Download', icon: 'fa-download' },
			'rename': { name: 'Rename', icon: 'edit' },
			'delete': { name: 'Delete', icon: 'delete' },
		}
	});

	/* Resize browser window
	 */
	var resize = 0;
	$(window).on('resize', function() {
		var current = ++resize;
		setTimeout(function() {
			if (current != resize) {
				return;
			}

			orb_desktop_rearrange();
		}, 100);
	});

	/* File changes
	 */
	orb_directory_upon_update(function(directory) {
		if (directory == 'Desktop') {
			orb_desktop_refresh();
		}
	});

	/* Drop files
	 */
	$('div.desktop').on('dragover', function(event) {
		var explorers = 0;
		$('div.explorer').each(function() {
			explorers++;
		});

		if (explorers == 0) {
			explorer_open('Desktop');
		}

		event.preventDefault();
		event.stopPropagation();
	});

	$('div.desktop').on('drop', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});
});
