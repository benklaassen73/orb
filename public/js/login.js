/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

$(document).ready(function() {
	$('input#username').keypress(function(event) {
		if (event.which == 13) {
			$('input#password').focus();
		}
	});

	$('input#password').keypress(function(event) {
		if (event.which == 13) {
			$('button').trigger('click');
		}
	});

	$('button').click(function() {
		var username = $('input#username').val();
		var password = $('input#password').val();

		$.post('/', {
			username: username,
			password: password
		}).done(function(data) {	
			window.location = '/';
		}).fail(function(result) {
			$('p.warning').remove();
			$('h1').after('<p class="warning">Invalid login.</p>');
			$('input#password').val('').focus();
		});
	});

	$('input#username').focus();
});
