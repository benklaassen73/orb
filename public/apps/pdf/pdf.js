/* Orb pdf application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use pdf_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

function pdf_open_file(pdf_window, filename) {
	var extension = orb_file_extension(filename);

	if (extension != 'pdf') {
		alert('Not a PDF file.');
		return;
	}

	pdf_window.find('iframe').attr('src', '/orb/file/download/' + filename);
}

function pdf_menu_click(pdf_window, item) {
	pdf_window = pdf_window.find('div.pdf');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				pdf_open_file(pdf_window, filename);
			});
			break;
		case 'Exit':
			pdf_window.close();
			break;
		case 'About':
			alert('PDF viewer\nCopyright (c) by Hugo Leisink');
			break;
	}
}

function pdf_open(filename = undefined) {
	var window_content =
		'<div class="pdf"><iframe></iframe></div>';

	var pdf_window = $(window_content).orb_window({
		header:'PDF',
		icon: '/apps/pdf/pdf.png',
		width: 500,
		height: 600,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: pdf_menu_click
	});

	pdf_window.open();

	if (filename != undefined) {
		pdf_open_file(pdf_window, filename);
	}
}

$(document).ready(function() {
    var icon = '/apps/pdf/pdf.png';
    orb_startmenu_add('PDF', icon, pdf_open);
    orb_upon_file_open('pdf', pdf_open, icon);
});
