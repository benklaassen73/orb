<?php
	/* Authentication
	 */
	require "../../../libraries/error.php";
	require "../../../libraries/general.php";

    session_name("Orb");
    session_start();

	$login = new login(null);
	if ($login->valid() == false) {
		return;
	}

	unset($_COOKIE["Orb"]);

	/* Browser
	 */
	require "http.php";
	require "proxy.php";

	if (isset($_GET["url"]) == false) {
		return;
	}


	list($protocol,, $hostname, $path) = explode("/", $_GET["url"], 4);
	$path = "/".str_replace(" ", "%20", $path);
	list($hostname, $port) = explode(":", $hostname, 2);

	/* Start proxy
	 */
	if ($protocol == "http:") {
		$proxy = new proxy($hostname, $port);
	} else if ($protocol == "https:") {
		$proxy = new proxys($hostname, $port);
	} else {
		return;
	}

	/* Forward request
	 */
	$result = $proxy->forward_request($path);

	switch ($result) {
		case CONNECTION_ERROR:
			print "Connection error.";
			break;
		case LOOPBACK:
			print "Loopback to Orb is not allowed.";
			break;
	}
?>
