/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_setting_color(term, parameters) {
	if (terminal_argument_count(term, parameters, 1) == false) {
		return
	}

	var color = parameters[0].toUpperCase();
	var format = /^#[0-9A-F]{6}$/g;

	if (format.test(color) == false) {
		term.writeln('Invalid color format.');
		terminal_done(term);
		return;
	}

	orb_setting_set('system/color', color, function() {
		orb_window_set_color(color);
		terminal_done(term);
	}, function() {
		term.writeln('Error setting color.');
		terminal_done(term);
	});
}

function terminal_setting_wallpaper(term, parameters) {
	if (terminal_argument_count(term, parameters, 1) == false) {
		return false;
	}

	var wallpaper = parameters[0];
	var extension = orb_file_extension(wallpaper);

	if (wallpaper.substr(0, 1) != '/') {
		wallpaper = term.path + '/' + wallpaper;
	}

	if (settings_wallpaper_extensions.includes(extension) == false) {
		term.writeln('Invalid wallpaper file.');
		terminal_done(term);
	} else {
		orb_setting_set('system/wallpaper', wallpaper, function() {
			orb_desktop_load_wallpaper(wallpaper);
			terminal_done(term);
		}, function() {
			term.writeln('Error setting wallpaper.');
			terminal_done(term);
		});
	}
}
