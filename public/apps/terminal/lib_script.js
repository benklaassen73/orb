/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_run_script(terminal_window, term, script) {
	var script_run = setInterval(function() {
		if (term.busy) {
			return;
		}

		if (script.length == 0) {
			clearInterval(script_run);
			terminal_window.close();
			return;
		}

		term.command = script.shift();

		terminal_command(terminal_window, term);
	}, 50);
}

function terminal_echo(term, parameters) {
	term.writeln(parameters.join(' '));
	terminal_done(term);
}

function terminal_anykey(terminal_window, term, parameters) {
	if (parameters.length > 0) {
		term.write(parameters.join(' '));
	} else {
		term.write('Press any key to continue...');
	}

	var listening = term.onKey(function(event) {
		term.writeln('');
		listening.dispose();
		terminal_done(term);
	});
}
