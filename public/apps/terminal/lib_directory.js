/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_directory_create(term, directories) {
	if (directories.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	var count = directories.length;

	directories.forEach(function(directory) {
		directory = terminal_combine_directories(term.path, directory);

		orb_directory_create(directory, function() {
			if (--count == 0) {	
				terminal_done(term);
			}
		}, function() {
			term.writeln('Error creating directory.');

			if (--count == 0) {	
				terminal_done(term);
			}
		});
	});
}

function terminal_directory_remove(term, directories) {
	if (directories.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	var count = directories.length;

	directories.forEach(function(directory) {
		directory = terminal_combine_directories(term.path, directory);

		orb_directory_remove(directory, function() {
			if (--count == 0) {
				terminal_done(term);
			}
		}, function() {
			term.writeln('Error removing directory.');

			if (--count == 0) {
				terminal_done(term);
			}
		});
	});
}
