/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_kill(term, parameters) {
	if (parameters.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	if ((parameters[0] == '-9') && (parameters[1] == '-1')) {
		$('div.desktop div.windows div.window').each(function() {
			$(this).close();
		});
		orb_logout();
		return;
	}

	parameters.forEach(function(pid) {
		$('div.desktop div.windows div#windowframe' + pid).close();
	});

	terminal_done(term);
}

function terminal_processes(term, parameters) {
	term.writeln(' PID  APPLICATION');
	$('div.desktop div.windows div.window').each(function() {
		var pid = $(this).prop('id').substr(11);
		var title = $(this).find('div.window-header div.title').text();

		term.writeln(String(pid).padStart(4, ' ') + '  ' + title);

	});

	terminal_done(term);
}
