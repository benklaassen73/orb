/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_sprintl(text, length) {
	var len = text.length;

	if (len > length) {
		return text.substr(0, length - 3) + '...';
	}

	return text.padEnd(length, ' ');
}

function terminal_sprintr(text, length) {
	var len = text.length;

	if (len > length) {
		return text.substr(-length);
	}

	return text.padStart(length, ' ');
}

function terminal_mkdate(timestamp) {
	var date = new Date(timestamp * 1000);
	var date_str = String(date.getDate()).padStart(2, '0') + "/" +
	               String(date.getMonth() + 1).padStart(2, '0') + "/" + 
	               String(date.getFullYear());

	return terminal_sprintr(date_str, 10);
}

function terminal_list(term, path = undefined) {
	if (path.length > 1) {
		term.writeln('Too many arguments.');
		terminal_done(term);
		return;
	} else if (path.length == 0) {
		path = term.path;
	} else {
		path = path[0];
		if (path.substr(0, 1) != '/') {
			path = term.path + '/' + path;
		}
	}

	orb_file_type(path, function(type) {
		var filename = null;
		if (type == 'file') {
			filename = orb_file_filename(path);
			path = orb_file_dirname(path);
		}

		orb_directory_list(path, function(items) {
			if (filename == null) {
				items.forEach(function(item) {
					if (item.type == 'directory') {
						var name = terminal_sprintl('\x1B[1;34m' + item.name + '\x1B[0m/', 69);
						var date = terminal_mkdate(item.create_timestamp);
						term.writeln(name + date);
					}
				});
			}
			items.forEach(function(item) {
				if ((filename != null) && (filename != item.name)) {
					return true;
				}

				if (item.type == 'file') {
					var name = terminal_sprintl(item.name, 58);
					var date = terminal_mkdate(item.create_timestamp);
					var size = terminal_sprintr(orb_file_nice_size(item.size), 12);
					term.writeln(name + date + size);
				}
			});
			term.writeln('  ' + items.length + ' files');

			terminal_done(term);
		}, function(result) {
			term.writeln('File or directory not found.');
			terminal_done(term);
		});
	}, function(result) {
		term.writeln('File or directory not found.');
		terminal_done(term);
	});
}
