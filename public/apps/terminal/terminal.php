<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class terminal extends orb_backend {
		/* DNS lookup
		 */
		public function get_dns() {
			$host = $this->parameters[0];

			if (valid_input($host, VALIDATE_NUMBERS.".:", VALIDATE_NONEMPTY)) {
				if (($host = gethostbyaddr($host)) == false) {
					$this->view->return_error(404);
				} else {
					$this->view->add_tag("result", $host);
				}
			} else if (valid_input($host, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS.".-_", VALIDATE_NONEMPTY)) {
				if (($record = dns_get_record($host, DNS_ALL)) == false) {
					$this->view->return_error(404);
				} else foreach ($record as $item) {
					$this->view->open_tag("result");
					foreach ($item as $key => $value) {
						if (is_array($value)) {
							continue;
						}
						$this->view->add_tag($key, $value);
					}
					$this->view->close_tag();
				}
			} else {
				$this->view->return_error(406);
			}
		}

		/* Ping
		 */
		public function get_ping() {
			$host = $this->parameters[0];

			if (valid_input($host, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS.".-_", VALIDATE_NONEMPTY) == false) {
				$this->view->return_error(406);
				return;
			}

			$lines = array();
			exec("ping -w".TERMINAL_NETWORK_TIMEOUT." ".$host, $lines);

			foreach ($lines as $line) {
				$this->view->add_tag("line", $line);
			}
		}

		/* Port reachability
		 */
		public function get_port() {
			$host = $this->parameters[0];
			$port = $this->parameters[1];

			if (dns_get_record($host) == false) {
				$this->view->return_error(404);
				return;
			}

			ob_start();
			$socket = fsockopen($host, $port, $errnum, $errstr, TERMINAL_NETWORK_TIMEOUT);
			ob_end_clean();

			if ($socket == false) {
				$this->view->return_error(403);
			} else {
				fclose($socket);
			}
		}
	}
?>
