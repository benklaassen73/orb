/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_show_help(term) {
	term.writeln('  cat <file> [...]            : Concatenate files and print to output.');
	term.writeln('  cd <directory>              : Change current directory.');
	term.writeln('  color #<RRGGBB>             : Change window color.');
	term.writeln('  cp <source> <destination>   : Copy the source file to the destination.')
	term.writeln('  clear                       : Clear terminal window.');
	term.writeln('  dns <hostname|IP address>   : DNS lookup.');
	term.writeln('  exit                        : Close terminal window.');
	term.writeln('  help                        : Show this help.');
	term.writeln('  kill <pid>                  : Terminate application.');
	term.writeln('  ls [<file|directory>]       : List file or directory content.');
	term.writeln('  mkdir <directory> [...]     : Create directory.');
	term.writeln('  mv <source> <destination>   : Move the source to the destination.')
	term.writeln('  open <file|directory> [...] : Open file or directory.');
	term.writeln('  ping <hostname|IP address>  : Ping host');
	term.writeln('  port <host> <port>          : Test whether port is open or closed.');
	term.writeln('  ps                          : Show process list.');
	term.writeln('  rm <file> [...]             : Remove file.');
	term.writeln('  rmdir <directory> [...]     : Remove directory.');
	term.writeln('  wallpaper <image file>      : Set wallpaper.');
}
