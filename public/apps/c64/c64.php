<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */
?>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
	html, body, div#canvas_container, canvas#canvas {
		width:100%;
		height:100%;
		margin:0;
		padding:0;
		overflow:hidden;
	}

	canvas#canvas {
		display:block;
		margin:0 auto;
	}
</style>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript">
	window.addEventListener('load', function () {
		window.focus();
		document.body.addEventListener('click', function(e) {
			window.focus();
		}, false);
	});

	function c64_global_startup() {
		Module.toggleMute();
	}

	function c64_drag_checkfile(file) {
		var validExt = ['S64', 'D64', 'G64', 'X64', 'ZIP', 'PRG', 'P00', 'T64', 'TAP', 'CRT'];
		var ext = file.name.split('.').pop();
		var exthi = ext.toUpperCase();
		for (var i=0; i < validExt.length; i++) {
			if ((ext == validExt[i]) || (exthi == validExt[i])) {
				return true;
			}
		}
		return false;
	}

	function open_file(content) {
		var bytes = new Uint8Array(content.length);
		for (var i = 0; i < content.length; i++) {
			bytes[i] = content.charCodeAt(i);
		}
		var blob = new Blob([bytes], { type: 'binary/octet-stream' });
		var file = new File([blob], "robb.d64", { type: 'binary/octet-stream' });

		if (c64_drag_checkfile(file)) {
			Module.loadFile(file, 1);
		}
	}

	/* Control functions
	 */
	function full_screen() {
		Module.requestC64FullScreen();
	}

	function toggle_sound() {
		Module.toggleMute();
	}

	function joystick(value) {
		Module.selectJoystick(value & 0xFF, value >> 8);
	}

	function joystick_on() {
		joystick(0x201D);
	}

	function joystick_off() {
		joystick(0);
	}

	/* Keys
	 */
	function key_down(value) {
		Module.setKey(value, 1);
	}

	function key_up(value) {
		Module.setKey(value, 0);
	}
</script>
</head>

<body>
<div>
	<canvas id="canvas"></canvas>

	<progress value="0" max="100" id="progress"></progress>
	<div id="status"></div>
	<div id="tape_keys"></div>
	<div id="c64TextInputReceiver" contentEditable="false" style="opacity: 0; color : transparent;"></div>
	<div id="highScore_title"></div>
	<div id="diskbox_combo"></div>

	<script type="text/javascript" src="/apps/c64/c64_tiny_host.js"></script>
	<script async type="text/javascript" src="/apps/c64/c64_tiny.js"></script>
</body>
</html>
