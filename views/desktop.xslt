<?xml version="1.0" ?>
<!--
	Copyright (c) by Hugo Leisink <hugo@leisink.net>
	This file is part of the Orb web desktop
	https://gitlab.com/hsleisink/orb

	Licensed under the GPLv2 License
-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="about:legacy-compat" />

<!-- Desktop -->
<xsl:template match="desktop">
<div class="desktop" version="{../@version}" debug="{../@debug}" mobile="{@mobile}" login="{../login/method}" timeout="{../login/timeout}">
	<div class="windows"></div>
	<div class="icons"></div>
	<div class="taskbar">
		<div class="start btn btn-primary btn-sm">Start</div>
		<div class="startmenu">
			<div class="system"></div>
			<div class="applications"></div>
		</div>
		<div class="tasks"></div>
	</div>
</div>
</xsl:template>

<!-- Missing -->
<xsl:template match="missing">
<script type="text/javascript">
$(document).ready(function() {
	alert('Orb encountered a configuration error. The application <xsl:value-of select="." /> is missing. Remove it from orb.conf.');
});
</script>
</xsl:template>

<!-- Output -->
<xsl:template match="/output">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale={desktop/@zoom}, maximum-scale={desktop/@zoom}" />
<meta name="generator" content="File" />
<link rel="apple-touch-icon" href="/images/orb.png" />
<link rel="icon" href="/images/orb.png" />
<link rel="shortcut icon" href="/images/orb.png" />
<title>Orb v<xsl:value-of select="@version" /></title>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}" /><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<xsl:apply-templates select="missing" />
<xsl:apply-templates select="desktop" />
</body>

</html>
</xsl:template>

</xsl:stylesheet>
